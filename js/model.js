class Product {
  constructor(n, id, pro, q, pri, c, del) {
    this.productNum = n;
    this.productId = id;
    this.product = pro;
    this.quantity = q;
    this.price = pri;
    this.country = c;
    this.deleteProduct = del;
  }
}
class Model {
  constructor() {
    this.products = [];
    this.productNum = 1;
  }
  addProductToArr(n, id, pro, q, pri, c, del) {
    let product = new Product(n, id, pro, q, pri, c, del);
    this.productNum++;
    this.products.push(product);
  }
  getProducts() {
    return this.products;
  }
  /*getOrder(id) {
      for (let i = 0; i < this.products.length; i++) {
          if (id === this.products[i].productId) {
              return this.products[i];
          }
      }
      return null;
  }*/
}