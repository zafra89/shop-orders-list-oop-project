let view = {
  'init': function() {
    this.giveFunctionalityToBtn();
  },
  'model': new Model(),
  'giveFunctionalityToBtn': function() {
    let addBtn = document.getElementById('addBtn');
    addBtn.addEventListener('click', this.addProduct);
  },
  'addProduct': function() {
    let productNum = view.model.productNum;
    let productID = Math.floor((Math.random() * 99999999999));
    let product = document.getElementById('product-name').value;
    let quantity = document.getElementById('quantity').value;
    let price = Math.floor((Math.random() * 400) + 10) + ' €';
    let country = document.getElementById('select-country').value;
    let deleteProduct = 'X';
    let pMsgError = document.getElementById('p-msg-error');
    if (product != '' && quantity != '' && country != '') {
      view.model.addProductToArr(productNum, productID, product, quantity, price, country, deleteProduct);
      view.updateList();
      view.clearInputs();
      pMsgError.innerHTML = '';
    } else {
      pMsgError.innerHTML = 'You must fill all the inputs';
    };
  },
  'clearInputs': function() {
    document.getElementById('product-name').value = '';
    document.getElementById('quantity').value = '';
    document.getElementById('select-country').value = '';
  },
  'updateList': function() {
    let tbodyProducts = document.getElementById('tbody_products');
    tbodyProducts.innerHTML = '';
    this.model.getProducts().forEach(product => {
      tbodyProducts.innerHTML += `
      <tr>
        <td>${product.productNum}</td>
        <td>${product.productId}</td>
        <td>${product.product}</td>
        <td>${product.quantity}</td>
        <td>${product.price}</td>
        <td>${product.country}</td>
        <td>${product.deleteProduct}</td>
      </tr>
      `;
    });
  }
}